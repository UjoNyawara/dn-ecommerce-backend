import { expect } from 'chai'
import MpesaService from '../MpesaService'
import { MpesaCredentials } from '../../types/payment'
import Nock from 'nock'
import { externalToken } from '../../middlewares/__tests__/__mocks__/data'

const CREDENTIALS: MpesaCredentials = {
  consumerKey: process.env.MPESA_CONSUMER_KEY,
  consumerSecret: process.env.MPESA_CONSUMER_SECRET,
}

const paymentDetails = {
  BusinessShortCode: '174379',
  TransactionType: 'CustomerPayBillOnline',
  Amount: '500',
  PartyA: '2348131976306',
  PartyB: '174379',
  PhoneNumber: '254708374149',
  CallBackURL: 'https://a8ba7fd3.ngrok.io/api/v1/payment/mpesa/callback',
  AccountReference: 'John Doe',
  TransactionDesc: 'test',
  Password: 'test',
  Timestamp: 'test',
}

describe('Unit Tests Mpesa Service', () => {
  const MPESA = new MpesaService(CREDENTIALS.consumerKey, CREDENTIALS.consumerSecret)
  it('Requires MPESA consumerKey and consumer secret parameters', async () => {
    /**
     * Catch error thrown from MPESA Servcie class
     * @returns {*} Response
     */
    async function throwError() {
      try {
        await MPESA.initializeLipaNaMPesaPayment(paymentDetails)
      } catch (e) {
        expect(e).to.exist
      }
    }
    await throwError()
  })
  it('Should throw error while getting client credentials', async (): Promise<void> => {
    before(async () => {
      Nock('https://sandbox.safaricom.co.ke')
        .get('/oauth/v1/generate?grant_type=client_credentials')
        .reply(200, undefined)
    })

    const auth = await MPESA.authenticateCredentials()
  })
})

describe('Unit Tests Mpesa Service fails when properties are not set correctly', () => {
  const MPESA = new MpesaService(CREDENTIALS.consumerKey, CREDENTIALS.consumerSecret)
  it('Requires MPESA consumerKey and consumer secret parameters', async () => {
    /**
     * Catch error thrown from MPESA Servcie class
     * @returns {*} Response
     */
    async function throwError() {
      try {
        MPESA.baseURL = undefined
        await MPESA.initializeLipaNaMPesaPayment(paymentDetails)
      } catch (error) {
        console.log(error)
        expect(error).to.exist
      }
    }
    await throwError()
  })
})

describe('Unit Tests Mpesa Service fails when properties are not set correctly', () => {
  it('Should return false if bearer token is not added', async () => {
    const MPESA = new MpesaService(CREDENTIALS.consumerKey, CREDENTIALS.consumerSecret)
    MPESA.accessToken = externalToken
    expect(MPESA.verifyToken()).to.be.true
  })
  it('Should return false if base url is not available', async () => {
    const MPESA = new MpesaService(CREDENTIALS.consumerKey, CREDENTIALS.consumerSecret)
    /**
     * Catch error thrown from MPESA Servcie class
     * @returns {*} Response
     */
    async function throwError() {
      try {
        MPESA.baseURL = undefined
        await MPESA.validateProps()
      } catch (error) {
        expect(error).to.exist
        expect(error.message).to.equal('Invalid MPESA API Base Url')
      }
    }
    await throwError()
  })
})
