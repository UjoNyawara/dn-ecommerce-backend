import chai, { expect, assert } from 'chai'
import path from 'path'
import fs from 'fs'
import { compileToTemplate, buildPDF } from '../PdfService'

describe('Functionality for generating PDF', () => {
  describe('Compiles source Handlebars template to HTML document', () => {
    it('Successfully compiles a template', async () => {
      const template = await compileToTemplate(
        await fs.readFileSync(path.resolve(__dirname, `./__mocks__/test.hbs`), 'utf-8'),
        { testname: 'compileToTemplate' },
      )
      expect(template).to.be.a('string')
    })

    it('Does not successfully compile a template', async () => {
      try {
        await compileToTemplate(undefined, { testname: 'compileToTemplate' })
      } catch (error) {
        expect(error.message).to.equal(
          'Error: You must pass a string or Handlebars AST to Handlebars.compile. You passed undefined',
        )
      }
    })

    it('Cannot build the PDF, because of invalid content', async () => {
      try {
        await buildPDF({})
      } catch (error) {
        expect(error.message).to.equal('Error: Invalid template content!')
      }
    })
  })
})
