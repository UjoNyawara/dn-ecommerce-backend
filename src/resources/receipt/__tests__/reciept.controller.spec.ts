import { expect } from 'chai'
import { mockReq, mockRes } from 'sinon-express-mock'
import ReceiptController from '../receipt.controller'
import { OK } from '../../../constants/statusCodes'
import db from '../../../database/models'
import { createOrder } from '../../order/__tests__/__mocks__/data'

let invoice
let invoiceId: string
let order

describe('Receipt Controller Main functionalities', () => {
  before(async () => {
    order = await db.Order.create(createOrder)
    order = order.get({ plain: true })
    invoice = await db['Invoice'].create({
      userId: 1923,
      orderId: order.id || '200',
      invoiceNumber: '#256' + new Date().getUTCMilliseconds(),
      amountToBePaid: 200,
      paymentStatus: 'pending',
    })
    invoice = invoice.get({ plain: true })
    invoiceId = invoice.id || ''
  })

  describe('Get Many Receipts Belonging to a USER', () => {
    it('Receipt Controller -> getMany', done => {
      const request = mockReq({
        currentUser: { id: 1923, admin: 'admin' },
      })
      const response = mockRes()
      ReceiptController.getMany(request, response).then(() => {
        expect(response.status).to.have.been.calledWith(OK)
        done()
      })
    })
  })

  describe('Download Recipts', () => {
    let findInvoice: any
    before(async () => {
      findInvoice = await db.Invoice.findOne({
        where: {
          id: invoiceId,
        },
        include: [
          {
            model: db['Order'],
            as: 'order',
            include: [
              {
                model: db['OrderItem'],
                as: 'orderItems',
                include: [
                  {
                    model: db['Product'],
                    as: 'product',
                    attributes: ['categoryId', 'productName', 'description', 'photos'],
                  },
                ],
              },
            ],
          },
        ],
      })
    })

    it('Get single receipt', done => {
      const request = mockReq({
        currentUser: { id: 1923, admin: 'admin' },
        invoice: findInvoice,
      })
      const response = mockRes()
      ReceiptController.getOne(request, response)

      expect(response.status).to.have.been.calledWith(OK)
      done()
    })

    it('Get a single receipt as PDF when billing is not supplied', done => {
      const altInvoice = findInvoice
      delete altInvoice.billing
      const request = mockReq({
        currentUser: { id: 1923, admin: 'admin' },
        invoice: altInvoice,
      })
      const response = mockRes()
      ReceiptController.downloadReceipt(request, response).then(() => {
        expect(response.status).to.have.been.calledWith(OK)
        done()
      })
    })

    it('Get a single receipt as PDF', done => {
      const request = mockReq({
        currentUser: { id: 1923, admin: 'admin' },
        invoice: findInvoice,
      })
      const response = mockRes()
      ReceiptController.downloadReceipt(request, response).then(() => {
        expect(response.status).to.have.been.calledWith(OK)
        done()
      })
    })
  })
})
