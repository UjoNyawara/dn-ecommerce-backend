import { expect } from 'chai'
import { mockReq, mockRes } from 'sinon-express-mock'
import { checkIfInvoicePaid } from '../middlewares/receipt.middleware'
import { FORBIDDEN } from '../../../constants/statusCodes'
import db from '../../../database/models'
import { createOrder } from '../../order/__tests__/__mocks__/data'
import sinon from 'sinon'

describe('Receipt Controller Main functionalities', () => {
  describe('Should validate the payment status of the invoice before returning a receipt/receipt PDF', () => {
    it('checkIfInvoicePaid: fails', async () => {
      let order: any = await db.Order.create(createOrder)
      order = order.get({ plain: true })
      let invoice: any = await db['Invoice'].create({
        userId: 1923,
        orderId: order.id || '200',
        invoiceNumber: '#256' + new Date().getUTCMilliseconds(),
        amountToBePaid: 200,
        paymentStatus: 'pending',
      })
      invoice = invoice.get({ plain: true })
      const request = mockReq({
        currentUser: { id: 1923, admin: 'admin' },
        invoice: invoice,
      })
      const response = mockRes()
      const next = sinon.fake()
      await checkIfInvoicePaid(request, response, next)
      expect(response.status).to.have.been.calledWith(FORBIDDEN)
    })

    it('checkIfInvoicePaid: passes', async () => {
      let order: any = await db.Order.create(createOrder)
      order = order.get({ plain: true })
      let invoice: any = await db['Invoice'].create({
        userId: 1923,
        orderId: order.id || '200',
        invoiceNumber: '#256' + new Date().getUTCMilliseconds(),
        amountToBePaid: 200,
        paymentStatus: 'paid',
      })
      invoice = invoice.get({ plain: true })
      const request = mockReq({
        currentUser: { id: 1923, admin: 'admin' },
        invoice: invoice,
      })
      const response = mockRes()
      const next = sinon.fake()
      await checkIfInvoicePaid(request, response, next)
      expect(next.calledOnce).to.be.true
    })
  })
})
